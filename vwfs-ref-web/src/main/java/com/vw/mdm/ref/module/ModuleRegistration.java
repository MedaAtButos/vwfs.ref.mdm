package com.vw.mdm.ref.module;

import com.orchestranetworks.module.ModuleContextOnRepositoryStartup;
import com.orchestranetworks.module.ModuleRegistrationServlet;
import com.orchestranetworks.module.ModuleServiceRegistrationContext;
import com.orchestranetworks.service.OperationException;

public class ModuleRegistration extends ModuleRegistrationServlet {

	private static final long serialVersionUID = 1818684201843882548L;

	@Override
	public void handleRepositoryStartup(ModuleContextOnRepositoryStartup pContext) throws OperationException {
	}

	@Override
	public void handleServiceRegistration(ModuleServiceRegistrationContext pContext) {
	}

}